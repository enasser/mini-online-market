package com.online.market.review.model;

public enum Rating {
    NONE,
    ONE_STAR,
    TWO_STAR,
    THREE_STAR,
    FOUR_STAR,
    FIVE_STAR
}
