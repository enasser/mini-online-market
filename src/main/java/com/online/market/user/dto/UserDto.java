package com.online.market.user.dto;

import com.online.market.role.dto.RoleDto;
import com.online.market.user.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserDto extends LightUserDto{

    private Date createdAt;

    private Date modifiedAt;

    private boolean isApprovedSeller;

    private Set<RoleDto> authorities;
}
