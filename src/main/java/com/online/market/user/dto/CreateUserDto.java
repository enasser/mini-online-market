package com.online.market.user.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor @AllArgsConstructor
public class CreateUserDto extends CreateLightUserDto {
    private Set<String> roles;
}
