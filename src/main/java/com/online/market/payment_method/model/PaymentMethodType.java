package com.online.market.payment_method.model;

public enum PaymentMethodType {
    NONE,
    CREDIT,
    DEBIT;
}
