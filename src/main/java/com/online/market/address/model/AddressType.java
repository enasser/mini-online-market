package com.online.market.address.model;

public enum AddressType {
    NONE,
    BILLING,
    SHIPPING;
}
