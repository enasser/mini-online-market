package com.online.market.address.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.online.market.address.model.AddressType;
import com.online.market.user.dto.UserDto;
import com.online.market.user.model.User;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AddressDto {
    private long id;

    @NotBlank
    private String street;

    @NotBlank
    private String city;

    private String state;

    @NotBlank
    private String zipCode;

    private AddressType type;

    @JsonBackReference
    private UserDto user;
}
