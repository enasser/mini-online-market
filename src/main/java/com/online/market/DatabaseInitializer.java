/*
package com.online.market;

import com.online.market.role.dto.RoleDto;
import com.online.market.role.model.Role;
import com.online.market.role.service.RoleService;
import com.online.market.user.dto.CreateUserDto;
import com.online.market.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@RequiredArgsConstructor
public class DatabaseInitializer implements ApplicationListener<ApplicationReadyEvent> {

    private final String[] usernames = {
            "admin",
            "seller",
            "buyer"
    };
    private final String[] names = {
            "Admin",
            "Seller",
            "Buyer"
    };
    private final String[] roles = {
            Role.ADMIN,
            Role.SELLER,
            Role.BUYER
    };
    private final String password = "p@$$w0rd";

    private final UserService userService;
    private final RoleService roleService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {

        // create user & roles
        for (String authority : roles) {
            RoleDto role = new RoleDto(authority);
            if (!roleService.existsByAuthority(role.getAuthority()))
                roleService.add(role);
        }

        for (int i = 0; i < usernames.length; ++i) {
            CreateUserDto newUser = new CreateUserDto();
            newUser.setUsername(usernames[i]);
            newUser.setEmail(usernames[i] + "@online-market.org");
            newUser.setFname(names[i]);
            newUser.setLname(names[i]);
            newUser.setPassword(password);
            newUser.setRePassword(password);

            newUser.setRoles(Set.of(roles[i]));

            if (!userService.existsByUsername(newUser.getUsername())) {

                if (usernames[i].equals("buyer"))
                    userService.addBuyer(newUser);
                else
                    userService.add(newUser);
            }
        }
    }
}
*/
