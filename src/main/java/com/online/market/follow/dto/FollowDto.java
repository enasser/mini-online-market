package com.online.market.follow.dto;

import com.online.market.user.model.User;
import lombok.*;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FollowDto {

    private Long id;

    private User follower;

    private User followee;
}
