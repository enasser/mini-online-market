package com.online.market.role.service;

import com.online.market.role.dto.RoleDto;

import java.util.List;

public interface RoleService {
    List<RoleDto> findAll();
    RoleDto findById(long id);

    RoleDto add(RoleDto dto);
    RoleDto updateById(long id, RoleDto dto);
    void deleteById(long id);

    boolean existsByAuthority(String name);
}
