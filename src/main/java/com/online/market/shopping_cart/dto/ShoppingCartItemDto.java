package com.online.market.shopping_cart.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.online.market.product.dto.ProductDto;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ShoppingCartItemDto {
    private long id;

    @JsonBackReference
    private ShoppingCartDto cart;

    private int quantity;

    private ProductDto product;
}
