package com.online.market.order.model;

public enum OrderStatus {
    PENDING,
    SHIPPED,
    ON_THE_WAY,
    DELIVERED,
    CANCELLED,
    RETURNED
}
