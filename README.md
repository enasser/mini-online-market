# Mini Online Market

Mini Online Market

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Create database

- [ ] mysql > [create database]() mini_online_market as name apear in `application.properties`  
```
create database mini_online_market 
```
***

# Run Maven Build
`using Intilij Editor `

## Run Application

## Brows `localhost8080/swagger-ui`
to see our endpoints

##To use emails 
you can add a valid email credentials in `application.properties`

`ex.`
`spring.mail.host=smtp.gmail.com
spring.mail.port=587
spring.mail.username=online-market@gmail.com
spring.mail.password=p@$$w0rd
`

## License
MIT.